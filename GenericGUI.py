import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QButtonGroup, QHBoxLayout, QPushButton


class GenericGUI(QWidget):
    def __init__(self, **kwarg):
        self.app = QApplication(sys.argv)
        super().__init__()
        self.title = kwarg['title'] if 'title' in kwarg else "Generic GUI"
        self.left = kwarg['left'] if 'left' in kwarg else 250
        self.top = kwarg['top'] if 'top' in kwarg else 250
        self.width = kwarg['width'] if 'width' in kwarg else 250
        self.height = kwarg['height'] if 'height' in kwarg else 250
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.aux_init(kwarg)

        self.init_ui()
        self.show()

    def aux_init(self, kwarg):
        # if 'test' in kwarg:
        #     print("HAHA!")
        None

    def init_ui(self):
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

    def add_button_box(self, rows=1, cols=1, names=None):
        button_layout = QVBoxLayout()
        self.button_list = []
        for row in range(rows):
            row_layout = QHBoxLayout()
            for col in range(cols):
                if names is not None:
                    s = str(names[row*rows+col])
                else:
                    s = "Button {}".format(str(row*rows+col+1))
                button = QPushButton(s)
                row_layout.addWidget(button)
                self.button_list.append(button)
            button_layout.addLayout(row_layout)

        self.layout.addLayout(button_layout)

    def bind_buttons(self, functions):
        for button, func in zip(self.button_list, functions):
            button.clicked.connect(func)

    def run(self):
        sys.exit(self.app.exec_())

if __name__ == "__main__":
    gui = GenericGUI(title="Test", width=360, height=360, test="BUBBLES!")
    gui.add_button_box(rows=2, cols=2, names=[1, 2, 3, 4])
    # gui.bind_buttons([test_function]*4)
    gui.run()
